﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lab1.Models
{
    public class Team
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Team Name")]
        public string TeamName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Established Date")]
        [DataType(DataType.Date)]
        public DateTimeOffset? EstablishedDate { get; set; }
    }
}
